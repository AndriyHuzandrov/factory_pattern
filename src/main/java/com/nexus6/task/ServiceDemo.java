package com.nexus6.task;

import com.nexus6.task.pizaa.PizzaType;
import com.nexus6.task.service.ServiceLviv;

public class ServiceDemo {
  
  public static void main(String[] args) {
    new ServiceLviv().createPizza(PizzaType.Pepperoni);
  }
}
