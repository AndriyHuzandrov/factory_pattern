package com.nexus6.task.pizaa;

import com.nexus6.task.compts.Addable;

public abstract class Pizza {
  Addable store;

  public Pizza(Addable st) {
    store = st;
  }

  abstract public void prepare();

  public void bake() {
    System.out.println("Temperature set at 300 degrees");
  }

  public void cut() {
    System.out.println("Your pizza is cut");
  }

  public void box() {
    System.out.println("Your pizza is in the box");
  }
}
