package com.nexus6.task.pizaa;

import com.nexus6.task.compts.Addable;

public class VeggiePizza extends Pizza {

  public VeggiePizza(Addable st) {
    super(st);
  }

  public void prepare() {
    store.addDough();
    store.addSauce();
    store.addCheese();
  }
}
