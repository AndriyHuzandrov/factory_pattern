package com.nexus6.task.service;

import com.nexus6.task.compts.Addable;
import com.nexus6.task.compts.KyivComponents;
import com.nexus6.task.pizaa.CheesePizza;
import com.nexus6.task.pizaa.ClamPizza;
import com.nexus6.task.pizaa.PepperoniPizza;
import com.nexus6.task.pizaa.Pizza;
import com.nexus6.task.pizaa.PizzaType;
import com.nexus6.task.pizaa.VeggiePizza;

public class ServiceKyiv extends Service {

  public Pizza createPizza(PizzaType type) {
    Addable store = new KyivComponents();
    switch(type) {
      case Pepperoni:
        return new PepperoniPizza(store);
      case Clam:
        return new ClamPizza(store);
      case Cheese:
        return new CheesePizza(store);
      case Veggie:
        return new VeggiePizza(store);
      default:
        System.out.println("No such pizza in out menu");
        return null;
    }
  }
}
