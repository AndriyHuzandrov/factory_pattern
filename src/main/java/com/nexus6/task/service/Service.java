package com.nexus6.task.service;

import com.nexus6.task.pizaa.Pizza;
import com.nexus6.task.pizaa.PizzaType;

public abstract class Service {

  Pizza orderPizza(PizzaType what) {
    Pizza pizza = null;
    pizza = createPizza(what);
    pizza.prepare();
    pizza.bake();
    pizza.cut();
    pizza.box();
    return pizza;
  }
  abstract Pizza createPizza(PizzaType type);
}
