package com.nexus6.task.service;

import com.nexus6.task.compts.Addable;
import com.nexus6.task.compts.LvivComponents;
import com.nexus6.task.pizaa.*;

public class ServiceLviv extends Service {

  public Pizza createPizza(PizzaType type) {
    Addable store = new LvivComponents();
    switch(type) {
      case Pepperoni:
        return new PepperoniPizza(store);
      case Clam:
        return new ClamPizza(store);
      case Cheese:
        return new CheesePizza(store);
      case Veggie:
        return new VeggiePizza(store);
      default:
        System.out.println("No such pizza in out menu");
        return null;
    }
  }
}
