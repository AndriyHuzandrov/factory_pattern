package com.nexus6.task.compts;

public class KyivComponents implements Addable {
  private int dough;
  private int sauce;
  private int cheese;

  public KyivComponents() {
    dough = 6;
    sauce = 7;
    cheese = 3;
  }

  public int addDough() {
    return dough;
  }

  public int addSauce() {
    return sauce;
  }

  public int addCheese() {
    return cheese;
  }
}
